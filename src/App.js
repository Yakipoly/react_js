import React, { useEffect, useState } from 'react';

import Login from './components/Login/Login';
import Home from './components/Home/Home';
import MainHeader from './components/MainHeader/MainHeader';
import Cookies from "js-cookie";

const AUTH_URL = 'http://127.0.0.1:8000/api-token-auth/'

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setIsLoggedIn(true);
    }
  }, []);

  const loginHandler = async (email, password) => {
    // We should of course check email and password
    // But it's just a dummy/ demo anyways
    // const { EntryEvent, getTokenName } = this.props;
    try {
      const headers = new Headers();
      headers.append("content-type", "application/json");
      console.log(email, password)
      const body = JSON.stringify({
        username: email,
        password: password,
      });

      const init = {
        method: "POST",
        headers,
        body,
      };

      const response = await fetch(AUTH_URL, init);
      let token = await response.json();
      if ("token" in token) {
        Cookies.set("isLoggedIn", true);
        localStorage.setItem('token', token.token);
      }
    } catch (err) {
      alert(err);
    }
  };

  

  const logoutHandler = () => {
    setIsLoggedIn(false);
    localStorage.removeItem('token');
  };

  return (
    <React.Fragment>
      <MainHeader isAuthenticated={isLoggedIn} onLogout={logoutHandler} />
      <main>
        {!isLoggedIn && <Login onLogin={loginHandler} />}
        {isLoggedIn && <Home onLogout={logoutHandler} />}
      </main>
    </React.Fragment>
  );
}

export default App;
