import React from 'react';

import Navigation from './Navigation';
import HeaderName from "./HeaderName"
import classes from './MainHeader.module.css';

const MainHeader = (props) => {
  return (
    <header className={classes['main-header']}>
      <HeaderName isLoggedIn={props.isAuthenticated}/>
      <Navigation isLoggedIn={props.isAuthenticated} onLogout={props.onLogout} />
    </header>
  );
};

export default MainHeader;
