import React from 'react';

import classes from './Navigation.module.css';

const Navigation = (props) => {
  return (
    <nav className={classes.nav}>
      <ul>
        {props.isLoggedIn && (
          <li>
            <a href="/">Automobiles</a>
          </li>
        )}
        {props.isLoggedIn && (
          <li>
            <a href="/spares">Spares</a>
          </li>
        )}
        {props.isLoggedIn && (
          <li>
            <a href="/manufacturer">Manufacturer</a>
          </li>
        )}
        {props.isLoggedIn && (
          <li>
            <button onClick={props.onLogout}>Logout</button>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default Navigation;
