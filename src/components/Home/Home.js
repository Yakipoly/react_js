import React from 'react';
import Card from '../UI/Card/Card';
import classes from './Home.module.css';


const URLS = {
  "/spares" : "http://127.0.0.1:8000/api/information/spares/",
  "/" : "http://127.0.0.1:8000/api/information/automobile/",
  "/manufacturer" : "http://127.0.0.1:8000/api/information/manufacturer/",
}

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      token: this.props.token,
      Elements: [],
    };
  }

  Elements = async () => {
    const token = "Token " + localStorage.getItem('token')
    try {
      const tresponse = await fetch(URLS[window.location.pathname], {
        headers: {
          Authorization: token,
          "content-type": "application/json",
        },
      });

      let Elements = await tresponse.json();

      return Elements;
    } catch (err) {
      alert(err);
    }
  };

  updateAllData = async () => {
    try {
      let Elements = await this.Elements();

      this.setState({ Elements: Elements });
    } catch (err) {
      alert(err);
    }
  };

  componentDidMount = async () => {
    this.updateAllData();
  };

  getText = (item) =>{
    if (window.location.pathname == "/") {
      return <div><h1>{item.title}</h1><p>Год: {item.year}</p></div>
    }
    if (window.location.pathname == "/spares") {
      return <div><h1>{item.name}</h1><p>Количество: {item.amount}</p></div>
    }
    if (window.location.pathname == "/manufacturer") {
      return <div><h1>{item.country}</h1></div>
    }
  }
  
  render() {
    console.log(this.state.Elements.results)
    return (
      <div>
        {JSON.stringify(this.state.Elements.results) && this.state.Elements.results.map(item=> (
        <Card className={classes.home} key={item.id}>
          { this.getText(item)}
        </Card>
        ))}
      </div>
    );
  }
}

